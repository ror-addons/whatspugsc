<?xml version="1.0" encoding="UTF-8"?>
<!--

-->
<ModuleFile xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance">
	<UiMod name="whatsPugSc" version="1.0" date="2017 july" >
		<VersionSettings gameVersion="" windowsVersion="1.0" savedVariablesVersion="1.0" />
		<Author name="anon" email="" />
		<Description text="Just a minor addon that gets the pug scenario from the message on logging in. Also makes the pug scenario appear first in scenario window. Command: /whatspugsc" />
		<Dependencies>
			<Dependency name="LibSlash" />
      		<Dependency name="EA_ChatWindow"/>
      		<Dependency name="EA_ChatSystem"/>
			<Dependency name="EA_ScenarioLobbyWindow" />
		</Dependencies>
		<Files>
			<File name="whatsPugSc.lua" />
		</Files>
		<SavedVariables>
			<SavedVariable name="whatsPugSc_SavedVariables" />
		</SavedVariables>
		<OnInitialize>
			<CallFunction name="whatsPugSc.OnInitialize" />
		</OnInitialize>
		<OnShutdown>
			<CallFunction name="whatsPugSc.OnShutdown" />
    	</OnShutdown>
	</UiMod>
</ModuleFile>
