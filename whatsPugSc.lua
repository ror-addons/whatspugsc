if not whatsPugSc then whatsPugSc = {} end

whatsPugSc.ScenarioInfo = 
-- this is for other languages
{
	-- tier 1
	["Gates of Ekrund"] 		= {name = GetScenarioName(2000), id = 2000},
	["Nordenwatch"] 			= {name = GetScenarioName(2100), id = 2100},
	["Khaine's Embrace"] 		= {name = GetScenarioName(2200), id = 2200},

	-- tier 2
	["Mourkain Temple"] 		= {name = GetScenarioName(2001), id = 2001},
	["Phoenix Gate"] 			= {name = GetScenarioName(2201), id = 2201},
	["Stonetroll Crossing"] 	= {name = GetScenarioName(2101), id = 2101},

	-- tier 3
	["Black Fire Basin"] 		= {name = GetScenarioName(2002), id = 2002},
	["Doomfist Crater"] 		= {name = GetScenarioName(2011), id = 2011},
	["Talabec Dam"] 			= {name = GetScenarioName(2102), id = 2102},
	["Highpass Cemetery"] 		= {name = GetScenarioName(2103), id = 2103},
	["Tor Anroc"] 				= {name = GetScenarioName(2202), id = 2202},
	["Lost Temple of Isha"] 	= {name = GetScenarioName(2203), id = 2203},
		
	-- tier 4
	["Thunder Valley"] 			= {name = GetScenarioName(2005), id = 2005},
	["Logrin's Forge"] 			= {name = GetScenarioName(2006), id = 2006},
	["Battle for Praag"] 		= {name = GetScenarioName(2106), id = 2106},
	["Grovod Caverns"] 			= {name = GetScenarioName(2107), id = 2107},
	["Serpent's Passage"] 		= {name = GetScenarioName(2204), id = 2204},
	["Dragon's Bane"] 			= {name = GetScenarioName(2205), id = 2205},

	-- tier 4, special
	["Altdorf War Quarters"]	= {name = GetScenarioName(2012), id = 2012},
	["The Undercroft"]			= {name = GetScenarioName(2013), id = 2013},
	["Maw of Madness"]			= {name = GetScenarioName(2104), id = 2104},
	["Reikland Hills"]			= {name = GetScenarioName(2109), id = 2109},

	-- tier 4, special
	["Kadrin Valley Pass"]		= {name = GetScenarioName(2003), id = 2003},
	["Gromril Crossing"]		= {name = GetScenarioName(2004), id = 2004},
	["Black Crag Keep"]			= {name = GetScenarioName(2007), id = 2007},
	["Howling Gorge"]			= {name = GetScenarioName(2008), id = 2008},
	["Karaz-a-Karak Gates"]		= {name = GetScenarioName(2009), id = 2009},
	["Eight Peaks Gates"]		= {name = GetScenarioName(2010), id = 2010},
	["Twisting Tower"]			= {name = GetScenarioName(2105), id = 2105},
	["Castle Fragendorf"]		= {name = GetScenarioName(2108), id = 2108},
	["Altdorf"]					= {name = GetScenarioName(2111), id = 2111},
	["Blood of the Black Cairn"]= {name = GetScenarioName(2206), id = 2206},
	["Caledor Woods"]			= {name = GetScenarioName(2207), id = 2207},
	["The Eternal Citadel"] 	= {name = GetScenarioName(2123), id = 2123},
	
}


function whatsPugSc.OnInitialize()
	whatsPugSc.sucess = false
	whatsPugSc.pugScenFirstHooked = false

	

	-- if whatsPugSc_SavedVariables == nil then
	-- 	whatsPugSc_SavedVariables = {}
	-- 	whatsPugSc_SavedVariables.PickupScenario = {name = L"", id = 1}
	-- else
	-- 	-- pug sc first
		
	-- end
	whatsPugSc_SavedVariables = whatsPugSc_SavedVariables or {}
	whatsPugSc_SavedVariables.PickupScenario.name = whatsPugSc_SavedVariables.PickupScenario.name or L""
	whatsPugSc_SavedVariables.PickupScenario.id = whatsPugSc_SavedVariables.PickupScenario.id or 1
	whatsPugSc_SavedVariables.showPickupScenarioFirst = whatsPugSc_SavedVariables.showPickupScenarioFirst or true
	whatsPugSc_SavedVariables.scenarioAutoLeave = whatsPugSc_SavedVariables.scenarioAutoLeave or true

	-- show pug sc first
	if whatsPugSc_SavedVariables.showPickupScenarioFirst then
		whatsPugSc.pugScenFirstSetHook(true)
	end

	--- autoleave
	whatsPugSc.scAutoLeaveHooked = false
	if whatsPugSc_SavedVariables.scenarioAutoLeave then
		whatsPugSc.scAutoLeaveSetHook(true)
	end

	whatsPugSc.starttime = GetComputerTime()
	TextLogAddEntry("Chat", 0, L"whatsPugSc started")
	RegisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "whatsPugSc.OnChat")
	LibSlash.RegisterSlashCmd("whatspugsc", function(input) whatsPugSc.HandleSlashCmd(input) end)

	--WindowRegisterEventHandler( "ScenarioSummaryWindow", SystemData.Events.SCENARIO_POST_MODE, "whatsPugSc.onScenarioPostMode" )
	
end

function whatsPugSc.HandleSlashCmd(input)
	if input == "" then
		TextLogAddEntry("Chat", 0, L"whatsPugSc status:")
		TextLogAddEntry("Chat", 0, L"* scenario: "..towstring(whatsPugSc_SavedVariables.PickupScenario.name)..L" id: "..towstring(tostring(whatsPugSc_SavedVariables.PickupScenario.id)))
		TextLogAddEntry("Chat", 0, L"* show pug sc first: "..towstring(tostring(whatsPugSc_SavedVariables.showPickupScenarioFirst)))
		TextLogAddEntry("Chat", 0, L"* autoleave sc: "..towstring(tostring(whatsPugSc_SavedVariables.scenarioAutoLeave)))
		TextLogAddEntry("Chat", 0, L"optional parameters: autoleave, showfirst")
	elseif input == "autoleave" then
		whatsPugSc_SavedVariables.scenarioAutoLeave = not whatsPugSc_SavedVariables.scenarioAutoLeave
		TextLogAddEntry("Chat", 0, L"* autoleave sc: "..towstring(tostring(whatsPugSc_SavedVariables.scenarioAutoLeave)))
	elseif input == "showfirst" then
		whatsPugSc_SavedVariables.showPickupScenarioFirst = not whatsPugSc_SavedVariables.showPickupScenarioFirst
		TextLogAddEntry("Chat", 0, L"* autoleave sc: "..towstring(tostring(whatsPugSc_SavedVariables.showPickupScenarioFirst)))
	end
end

function whatsPugSc.OnShutdown()
	UnregisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "whatsPugSc.OnChat")
end

--- getting scenario name and id
function whatsPugSc.OnChat()
	local type = GameData.ChatData.type

	if (type == SystemData.ChatLogFilters.SCENARIO) then
		whatsPugSc.sucess = whatsPugSc.ChatHandler()
	else
		if not whatsPugSc.sucess and (GetComputerTime() - whatsPugSc.starttime) > 30 then
			UnregisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "whatsPugSc.OnChat")
			TextLogAddEntry("Chat", 0, L"whatsPugSc has given up. maybe it's okay like that")
		end
	end
	
end

function whatsPugSc.ChatHandler()
	
	local msg = GameData.ChatData.text
	
	if (wstring.sub(msg, 1, 31) == L"The current pickup scenario is ") then
		local nn = wstring.sub(msg, 32, -2) -- will stop working if ROR staff changes the message format obviously
		--L"The current pickup scenario is Nordenwatch."

		whatsPugSc_SavedVariables.PickupScenario = {}
		whatsPugSc_SavedVariables.PickupScenario.name = nn
		whatsPugSc_SavedVariables.PickupScenario.id = whatsPugSc.getScenarioId(nn)

		if whatsPugSc_SavedVariables.PickupScenario.id ~= 1 then
			TextLogAddEntry("Chat", 0, towstring("whatspugsc: gotcha! current pug scenario is "..tostring(whatsPugSc_SavedVariables.PickupScenario.name)))

			if whatsPugSc_SavedVariables.showPickupScenarioFirst then
				whatsPugSc.pugScenFirstSetHook(true)
			end
			return true

		else
			TextLogAddEntry("Chat", 0, L"whatspugsc: sorry i tried")
		end
		UnregisterEventHandler(SystemData.Events.CHAT_TEXT_ARRIVED, "whatsPugSc.OnChat")
	end
	return false
end

function whatsPugSc.getScenarioId(scname)
	return (whatsPugSc.ScenarioInfo[tostring(whatsPugSc_SavedVariables.PickupScenario.name)].id or 1)
end

--- pug scenario first
function whatsPugSc.pugScenFirstSetHook(enable)
	if enable and not whatsPugSc.pugScenFirstHooked then
		whatsPugSc.pugScenFirstHooked = true
		whatsPugSc.UpdateQueueListHook = EA_Window_ScenarioLobby.UpdateQueueList
		EA_Window_ScenarioLobby.UpdateQueueList = whatsPugSc.UpdateQueueList
	elseif not enable and whatsPugSc.pugScenFirstHooked then
		whatsPugSc.pugScenFirstHooked = false
		EA_Window_ScenarioLobby.UpdateQueueList = whatsPugSc.UpdateQueueListHook
	end
end

function whatsPugSc.UpdateQueueList()
	whatsPugSc.UpdateQueueListHook()
	for i, sc in ipairs(GameData.ScenarioQueueData) do
		if (GetScenarioName(sc.id) == whatsPugSc_SavedVariables.PickupScenario.name) or (sc.id == whatsPugSc_SavedVariables.PickupScenario.id) then
			EA_Window_ScenarioLobby.ShowScenario( i )
		end
	end
end

--- auto leave
function whatsPugSc.scAutoLeaveSetHook(enable)
	if enable and not whatsPugSc.scAutoLeaveHooked then
		whatsPugSc.scAutoLeaveHooked = true
		whatsPugSc.ShowOrHideLeaveNowButtonHook = ScenarioSummaryWindow.ShowOrHideLeaveNowButton
		ScenarioSummaryWindow.ShowOrHideLeaveNowButton = whatsPugSc.ShowOrHideLeaveNowButton
	elseif not enable and whatsPugSc.scAutoLeaveHooked then
		whatsPugSc.scAutoLeaveHooked = false
		ScenarioSummaryWindow.ShowOrHideLeaveNowButton = whatsPugSc.ShowOrHideLeaveNowButtonHook
	end
end

function whatsPugSc.ShowOrHideLeaveNowButton()
    whatsPugSc.ShowOrHideLeaveNowButtonHook()
    local shouldShow = GameData.ScenarioData.mode == GameData.ScenarioMode.POST_MODE and ScenarioSummaryWindow.currentMode == ScenarioSummaryWindow.MODE_POST_MODE and not GameData.Player.isInSiege
    if (shouldShow) then
        BroadcastEvent( SystemData.Events.SCENARIO_FINAL_SCOREBOARD_CLOSED )
    end
end

--- not used
function whatsPugSc.onScenarioPostMode()
    BroadcastEvent( SystemData.Events.SCENARIO_FINAL_SCOREBOARD_CLOSED )
end